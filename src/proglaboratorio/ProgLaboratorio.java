package proglaboratorio;

import java.util.Scanner;

public class ProgLaboratorio {

    private static Laboratorio lab;
    private static Scanner s;

    public static void main(String[] args) {
        lab = new Laboratorio();
        s = new Scanner(System.in).useDelimiter(System.lineSeparator());
        System.out.print("Salvare (s) o caricare(l): ");
        String scelta = s.nextLine();
        System.out.print("Nome del file: ");
        String fileName = s.nextLine();
        switch (scelta) {
            case "s" : {
                save(fileName);
                break;
            }
            case "l": {
                load(fileName);
                break;
            }
        }
    }

    private static void save(String filename) {
        Laptop l = new Laptop("Huawei", 16, Math.random() > 0.5, 15.8);
        Server s = new Server("Dell", 128, Math.random() > 0.5, 5);
        Desktop d = new Desktop("HP", 8, true, 1);
        lab.add(l);
        lab.add(s);
        lab.add(d);
        System.out.println(lab);
        lab.sort();
        System.out.println(lab);
        lab.save(filename);
    }

    private static void load(String filename) {
        lab.load(filename);
        System.out.println(lab);
    }
}