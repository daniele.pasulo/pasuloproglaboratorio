package proglaboratorio;

public class Server extends PC {

    private int numeroCPU;

    public Server(String marca, double memoria, boolean guasto, int numeroCPU) {
        super(marca, "Server", memoria, guasto);
        if (numeroCPU <= 0 ) {
            numeroCPU = 1;
        }
        this.numeroCPU = numeroCPU;
    }

    public Server(Server s) {
        this(s.marca, s.memoria, s.guasto, s.numeroCPU);
    }

    public int getNumeroCPU() {
        return numeroCPU;
    }

    public void setNumeroCPU(int numeroCPU) {
        if (numeroCPU > 0)
            this.numeroCPU = numeroCPU;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = super.equals(obj);
        if (equal && obj instanceof Server) {
            equal = ((Server) obj).numeroCPU == numeroCPU;
        }
        else {
            equal = false;
        }
        return equal;
    }

    @Override
    public String toString() {
        return super.toString()+"Numero di CPU: "+numeroCPU+System.lineSeparator();
    }

    @Override
    public PC clone() {
        return new Server(this);
    }
}
