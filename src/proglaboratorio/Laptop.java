package proglaboratorio;

public class Laptop extends PC {

    private double dimensioneSchermo;

    public Laptop(String marca, double memoria, boolean guasto, double dimensioneSchermo) {
        super(marca, "Laptop", memoria, guasto);
        setDimensioneSchermo(dimensioneSchermo);
    }

    public Laptop(Laptop l) {
        this(l.marca, l.memoria, l.guasto, l.dimensioneSchermo);
    }

    public double getDimensioneSchermo() {
        return dimensioneSchermo;
    }

    public void setDimensioneSchermo(double dimensioneSchermo) {
        if (dimensioneSchermo > 0 )
            this.dimensioneSchermo = dimensioneSchermo;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = super.equals(obj);
        if (equal && obj instanceof Laptop) {
            equal = ((Laptop) obj).dimensioneSchermo == dimensioneSchermo;
        }
        else {
            equal = false;
        }
        return equal;
    }

    @Override
    public String toString() {
        return super.toString()+"Dimensioni schermo: "+String.format("%.2f", dimensioneSchermo)+"\""+System.lineSeparator();
    }

    @Override
    public PC clone() {
        return new Laptop(this);
    }
}
