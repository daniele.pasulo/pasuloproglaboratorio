package proglaboratorio;

public class Desktop extends PC {

	//this = Desktop
	//super = PC
    private int numeroNIC;

    public Desktop(String marca, double memoria, boolean guasto, int numeroNIC) {
        super(marca, "Desktop", memoria, guasto);
        this.numeroNIC = numeroNIC;
    }

    public Desktop(Desktop d) {
        this(d.marca, d.memoria, d.guasto, d.numeroNIC);
    }


    public int getNumeroNIC() {
        return numeroNIC;
    }

    public void setNumeroNIC(int numeroNIC) {
        this.numeroNIC = numeroNIC;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = super.equals(obj);
        if (equal && obj instanceof Desktop) {
            equal = ((Desktop) obj).numeroNIC == numeroNIC;
        }
        else {
            equal = false;
        }
        return equal;
    }

    @Override
    public String toString() {
        return super.toString()+"Numero di NIC: "+numeroNIC+System.lineSeparator();
    }

    @Override
    public PC clone() {
        return new Desktop(this);
    }
}
