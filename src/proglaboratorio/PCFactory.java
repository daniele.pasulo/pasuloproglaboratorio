package proglaboratorio;

public class PCFactory {

    public static PC fromCSVLine(String[] line) {
        String marca = line[0];
        double memoria = Double.parseDouble(line[2]);
        boolean guasto = Boolean.parseBoolean(line[3]);
        PC pc = null;
        switch (line[1]) {
            case "Server": {
                pc = new Server(marca, memoria, guasto, Integer.parseInt(line[4]));
                break;
            }
            case "Laptop": {
                pc = new Laptop(marca, memoria, guasto, Double.parseDouble(line[4]));
                break;
            }
            case "Desktop": {
                pc = new Desktop(marca, memoria, guasto, Integer.parseInt(line[4]));
                break;
            }
        }
        return pc;
    }
}
