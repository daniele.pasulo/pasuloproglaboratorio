package proglaboratorio;

import java.io.Serializable;

public abstract class PC implements Serializable, Comparable<PC> {
    private static final long serialVersionUID = 1L;

    protected String marca;
    protected String tipo;
    protected double memoria;
    protected boolean guasto;

    public PC(String marca, String tipo, double memoria, boolean guasto) {
        this.marca = marca;
        this.tipo = tipo;
        if (memoria <= 0) {
            memoria = 2;
        }
        this.memoria = memoria;
        this.guasto = guasto;
    }

    public PC(PC pc) {
        this(pc.marca, pc.tipo, pc.memoria, pc.guasto);
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getMemoria() {
        return memoria;
    }

    public void setMemoria(double memoria) {
        if (memoria > 0)
            this.memoria = memoria;
    }

    public boolean isGuasto() {
        return guasto;
    }

    public void setGuasto(boolean guasto) {
        this.guasto = guasto;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (this == obj) {
            equal = true;
        } else if (obj instanceof PC) {
            PC pc = (PC) obj;
            equal = pc.guasto == guasto && pc.memoria == memoria &&
                    pc.marca.equals(marca) && pc.tipo.equals(tipo);
        }
        return equal;
    }

    @Override
    public String toString() {
        String s = "Marca: " + marca + System.lineSeparator();
        s += "Tipo: " + tipo + System.lineSeparator();
        s += "Memoria: " + memoria + "GB" + System.lineSeparator();
        s += "Guasto: " + (guasto ? "sì" : "no") + System.lineSeparator();
        return s;
    }

    @Override
    public int compareTo(PC other) {
        return marca.compareTo(other.marca);
    }

    @Override
    public abstract PC clone();
}
