package proglaboratorio;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Laboratorio {

    private ArrayList<PC> computers;

    public Laboratorio(int nPC) {
        this.computers = new ArrayList<>(nPC);
    }

    public Laboratorio() {
        this(10);
    }

    public void add(PC pc) {
        computers.add(pc.clone());
    }

    public boolean elimina(PC pc) {
        return computers.remove(pc);
    }

    public String elencoPCGuasti() {
        int totPCGuasti = 0;
        String s = "";
        for (PC pc : computers) {
            if (pc.isGuasto()) {
                s += pc + System.lineSeparator();
                totPCGuasti++;
            }
        }
        return "Totale PC guasti: " + totPCGuasti + System.lineSeparator() + s;
    }

    @Override
    public String toString() {
        String s = "";
        for (PC pc : computers) {
            s += pc + System.lineSeparator();
        }
        return s;
    }

    public boolean save(String fileName) {
        boolean success;
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))) {
            out.writeObject(computers);
            out.flush();
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    @SuppressWarnings("unchecked")
    public boolean load(String fileName) {
        boolean success;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            computers = (ArrayList<PC>) in.readObject();
            success = true;
        } catch (IOException | ClassCastException | ClassNotFoundException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public boolean loadCSV(String fileName) {
        boolean success;
        ArrayList<PC> pcs = new ArrayList<>();
        try (Scanner s = new Scanner(new File(fileName)).useDelimiter(System.lineSeparator())) {
            while (s.hasNext()) {
                pcs.add(PCFactory.fromCSVLine(s.nextLine().split(";")));
            }
            computers = pcs;
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public void sort() {
        computers.sort((pc1, pc2) -> pc1.compareTo(pc2));
    }
}